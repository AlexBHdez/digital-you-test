import Rellax from 'rellax';

import './scss/main.scss';

import './components/header/header';
import './components/intro/intro';
import './components/projects/projects';
import './components/more-projects/more_projects';
import './components/loader/loader';

const rellax = new Rellax('.rellax', {
  center: false,
  round: true,
  vertical: true,
  horizontal: false
});

console.log('digitalYou Ready!!!');
