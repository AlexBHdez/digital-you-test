let myTime;
let loader = document.querySelector('.loader');
let intro = document.querySelector('.content-wrapper');

document.body.onload = () => {
  myTime = setTimeout(showPage, 2000);
}

function showPage() {
  intro.style.opacity = 1;
  loader.style.display = 'none';
}