import bgImage from './../../assets/img/intro/home_images_01.jpg';

let introSection = document.querySelector('.intro');
// Set the bg image to the intro section
introSection.style.backgroundImage = `url(${bgImage})`;