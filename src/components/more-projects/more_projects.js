let arrow = document.querySelectorAll('.arrow-wrapper *');
let arrowWrapper = document.querySelector('.arrow-wrapper');
let body = document.querySelector('body');
let contactBubble = document.querySelector('#bubble');
let contactCircle = document.querySelector('#circle');
let contactLines = document.querySelector('#bubble-lines');
let moreProject = document.querySelector('.toHover');

// Action over the moreProject Text
moreProject.addEventListener('mouseover', (e) => {
  let element = e.currentTarget;
  let arrowPos = window.getComputedStyle(arrowWrapper).left;

  arrowWrapper.style.left = `calc(${arrowPos} + 20px)`;
  body.style.backgroundColor = '#fff';
  contactBubble.style.fill = '#fff';
  contactCircle.style.fill = '#263265';
  contactLines.style.fill = '#263265';
  element.style.color = '#263265';

  // To paint all the elements of the arrow.svg once the mouse is over the element
  for (let tag of arrow) {
    tag.style.fill = '#263265';
    tag.style.stroke = '#263265';
  }
})

// Action out the moreProject Text
moreProject.addEventListener('mouseout', (e) => {
  let element = e.currentTarget;
  let arrowPos = window.getComputedStyle(arrowWrapper).left;

  arrowWrapper.style.left = `calc(${arrowPos} - 20px)`;
  body.style.backgroundColor = '#263265';
  contactBubble.style.fill = '#263265';
  contactCircle.style.fill = '#fff';
  contactLines.style.fill = '#fff';
  element.style.color = '#fff';
  
  // To paint all the elements of the arrow.svg once the mouse is out the element
  for (let tag of arrow) {
    tag.style.fill = '#fff';
    tag.style.stroke = '#fff';
  }
})
