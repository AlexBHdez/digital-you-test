import image1 from './../../assets/img/highlighted/highlighted_01.jpg';
import image2 from './../../assets/img/highlighted/highlighted_02.svg';
import image3 from './../../assets/img/highlighted/highlighted_03.jpg';
import image4 from './../../assets/img/highlighted/highlighted_04.jpg';
import image5 from './../../assets/img/highlighted/highlighted_05.jpg';
import image6 from './../../assets/img/highlighted/highlighted_06.jpg';

// array of the imported images
let imagesArray = [image1, image2, image3, image4, image5, image6];
let projects = document.querySelectorAll('.highlighted-wrapper__item');

// One image for each div
for (let [index, project] of projects.entries()) {
  project.style.backgroundImage = `url(${imagesArray[index]})`;
}


// selector of all the divs with overlay
let projectsOverlay = document.querySelectorAll('.toOverlay');

// over event for each div to show the overlay and the text content moving up and changing the opacity
for (let [index, project] of projectsOverlay.entries()) {
  project.addEventListener('mouseover', function (e) {
    let overlay = e.currentTarget.firstElementChild.firstElementChild;
    let text = e.currentTarget.firstElementChild.firstElementChild.firstElementChild;

    text.style.opacity = 1;
    text.style.top = '0px';
    
    if (index == 0) {
      overlay.style.backgroundColor = 'rgba(128, 130, 140, 0.8)';
    } else if (index == 1) {
      overlay.style.backgroundColor = 'rgba(185, 152, 104, 0.8)';
    } else {
      overlay.style.backgroundColor = 'rgba(222, 142, 124, 0.8)';
    }
  })
}

// out mouse event for each div to go back to initial position
for (let project of projectsOverlay) {
  project.addEventListener('mouseout', function (e) {
    let overlay = e.currentTarget.firstElementChild.firstElementChild;
    let text = e.currentTarget.firstElementChild.firstElementChild.firstElementChild;
    let image = e.currentTarget.firstElementChild.lastElementChild.firstElementChild;

    text.style.opacity = 0;
    text.style.top = '50px';

    overlay.style.backgroundColor = 'rgba(128, 130, 140, 0)';
  })
}






