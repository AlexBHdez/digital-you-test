// Show and hide mobile menu
let burgerIcon = document.querySelector('.burger');
let navMobile = document.querySelector('.nav-mobile');
let firstLine = document.querySelector('#first-line');
let lastLine = document.querySelector('#last-line');

// Clic en el burgerIcon
burgerIcon.addEventListener('click', () => {
  document.body.scroll = 'none';
  // show or hide mobile menu and burger icon animation
  window.getComputedStyle(navMobile).display === 'none' ? (
    navMobile.style.display = 'block',
    firstLine.style.transform = 'translateY(7px)',
    lastLine.style.transform = 'translateY(-7px)'
  ) : (
    navMobile.style.display = 'none',
    firstLine.style.transform = 'translateY(0px)',
    lastLine.style.transform = 'translateY(0px)'
  )

});

// Header & scrollLine
let prevScrollPos = window.pageYOffset;
let header = document.querySelector('.header-wrapper');
let heightHeader = window.getComputedStyle(header).height;
let scrollWrapper = document.querySelector('.scroll-wrapper__item');

window.onscroll = () => {
  let currentScrollPos = window.pageYOffset;
  
  prevScrollPos > currentScrollPos ?
    (
      header.style.top = '0',
      header.style.backgroundColor = '#263265',
      header.style.height = '80px'
    )
  :
    header.style.top = `-${heightHeader}`;
    
  prevScrollPos = currentScrollPos;
  
  if (currentScrollPos <= 100) {
    header.style.backgroundColor = 'transparent';
    header.style.height = '120px'
    scrollWrapper.classList.remove('hideLine');
  } else {
    scrollWrapper.classList.add('hideLine');
  }

}